<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!-- ./index.qdoc -->
<head>
  <title>Location Library Developer Resources</title>
  <link href="classic.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="left" valign="top" width="32"><img src="images/qtlogo.png" align="left" border="0" /></td>
<td width="1">&nbsp;&nbsp;</td><td class="postheader" valign="center"><a href="index.html"><font color="#004faf">Home</font></a></td>
<td align="right" valign="top" width="230"><img src="images/codeless.png"  border="0" /></td></tr></table><h1 class="title">Location Library Developer Resources<br /><span class="subtitle"></span>
</h1>
<p>The Location API represents a position on the Earth at a particular latitude and longitude. Other data specifying the time, altitude, velocity and bearing are also included.</p>
<ul><li><a href="#introduction">Introduction</a></li>
<li><a href="#requirements">Requirements</a></li>
<li><a href="#overview">Overview</a></li>
<li><a href="#requesting-location-data-from-data-sources">Requesting location data from data sources</a></li>
<ul><li><a href="#controlling-aspects-of-data-sources">Controlling aspects of data sources</a></li>
<li><a href="#reading-nmea-data">Reading NMEA data</a></li>
</ul>
<li><a href="#example-logfile-position-source-creating-a-custom-location-data-source">Example: Logfile Position Source - Creating a custom location data source</a></li>
</ul>
<a name="introduction"></a>
<h3>Introduction</h3>
<p>Location data involves a precisely specified position on the Earth's surface &#x2014; as provided by a latitude-longitude coordinate &#x2014; along with associated data, such as:</p>
<ul>
<li>The date and time at which the position was reported</li>
<li>The velocity of the device that reported the position</li>
<li>The altitude of the reported position (height above sea level)</li>
<li>The bearing of the device in degrees, relative to true north</li>
</ul>
<p>This data can be extracted through a variety of methods. One of the most well known methods of positioning is GPS (Global Positioning System), a publicly available system that uses radiowave signals received from Earth-orbiting satellites to calculate the precise position and time of the receiver. Another popular method is Cell ID positioning, which uses the cell ID of the cell site that is currently serving the receiving device to calculate its approximate location. These and other positioning methods can all be used with the Location API; the only requirement for a location data source within the API is that it provides a latitude-longitude coordinate with a date/time value, with the option of providing the other attributes listed above.</p>
<a name="requirements"></a>
<h3>Requirements</h3>
<p>This library requires Qt 4.5 to be installed.</p>
<p>To build the library, run <tt>qmake</tt> and <tt>make</tt>, then <tt>make install</tt>.</p>
<a name="overview"></a>
<h3>Overview</h3>
<p>The main API classes are:</p>
<ul>
<li><a href="qgeocoordinate.html">QGeoCoordinate</a></li>
<li><a href="qgeopositioninfo.html">QGeoPositionInfo</a></li>
<li><a href="qgeopositioninfosource.html">QGeoPositionInfoSource</a></li>
<li><a href="qgeoareamonitor.html">QGeoAreaMonitor</a></li>
<li><a href="qgeosatelliteinfo.html">QGeoSatelliteInfo</a></li>
<li><a href="qgeosatelliteinfosource.html">QGeoSatelliteInfoSource</a></li>
</ul>
<p>Location data sources are created by subclassing <a href="qgeopositioninfosource.html">QGeoPositionInfoSource</a> and providing <a href="qgeopositioninfo.html">QGeoPositionInfo</a> objects through the <a href="qgeopositioninfosource.html#positionUpdated">QGeoPositionInfoSource::positionUpdated</a>() signal. Clients that require location data can connect to the <a href="qgeopositioninfosource.html#positionUpdated">positionUpdated()</a> signal and call <a href="qgeopositioninfosource.html#startUpdates">startUpdates()</a> or <a href="qgeopositioninfosource.html#requestUpdate">requestUpdate()</a> to trigger the distribution of location data.</p>
<p>A default position source may be available on some platforms. Call QGeoPositionInfoSource::createSource() to create an instance of the default position source; the method returns 0 if no default source is available for the platform.</p>
<p>The <a href="qgeoareamonitor.html">QGeoAreaMonitor</a> class enables client applications to be notified when the receiving device has moved in or out of a particular area, as specified by a coordinate and radius. If the platform provides built-in support for area monitoring, QGeoAreaMonitor::createMonitor() returns an instance of the default area monitor.</p>
<p>Satellite information can also be distributed through the <a href="qgeosatelliteinfosource.html">QGeoSatelliteInfoSource</a> class. Call QGeoSatelliteInfoSource::createSource() to create an instance of the default satellite data source for the platform, if one is available. Alternatively, clients can subclass it to provide a custom satellite data source.</p>
<a name="requesting-location-data-from-data-sources"></a>
<h3>Requesting location data from data sources</h3>
<p>To receive data from a source, connect to its <a href="qgeopositioninfosource.html#positionUpdated">positionUpdated()</a> signal, then call either <a href="qgeopositioninfosource.html#startUpdates">startUpdates()</a> or <a href="qgeopositioninfosource.html#requestUpdate">requestUpdate()</a> to begin.</p>
<p>Here is an example of a client that receives data from the default location data source, as returned by QGeoPositionInfoSource::createSource():</p>
<pre>    class MyClass : public QObject
    {
        Q_OBJECT
    public:
        MyClass(QObject *parent = 0)
            : QObject(parent)
        {
            QGeoPositionInfoSource *source = QGeoPositionInfoSource::createSource();
            if (source) {
                connect(source, SIGNAL(positionUpdated(QGeoPositionInfo)),
                        this, SLOT(positionUpdated(QGeoPositionInfo)));
                source-&gt;startUpdates();
            }
        }

    private slots:
        void positionUpdated(const QGeoPositionInfo &amp;info)
        {
            qDebug() &lt;&lt; &quot;Position updated:&quot; &lt;&lt; info;
        }
    };</pre>
<a name="controlling-aspects-of-data-sources"></a>
<h4>Controlling aspects of data sources</h4>
<p>The <a href="qgeopositioninfosource.html#updateInterval-prop">QGeoPositionInfoSource::setUpdateInterval</a>() method can be used to control the rate at which position updates are received. For example, if the client application only requires updates once every 30 seconds, it can call <tt>setUpdateInterval(30000)</tt>. (If no update interval is set, or setUpdateInterval() is called with a value of 0, the source uses a default interval or some other internal logic to determine when updates should be provided.)</p>
<p><a href="qgeopositioninfosource.html#setPreferredPositioningMethods">QGeoPositionInfoSource::setPreferredPositioningMethods</a>() enables client applications to request that a certain type of positioning method be used. For example, if the application prefers to use only satellite positioning, which offers fairly precise outdoor positioning but can be a heavy user of power resources, it can call this method with the <a href="qgeopositioninfosource.html#PositioningMethod-enum">QGeoPositionInfoSource::SatellitePositioningMethods</a> value. However, this method should only be used in specialized client applications; in most cases, the default positioning methods should not be changed, as a source may internally use a variety of positioning methods that can be useful to the application.</p>
<a name="reading-nmea-data"></a>
<h4>Reading NMEA data</h4>
<p><a href="http://en.wikipedia.org/wiki/NMEA_0183">NMEA</a> is a common text-based protocol for specifying navigational data. For convenience, the <a href="qnmeapositioninfosource.html">QNmeaPositionInfoSource</a> is provided to enable client applications to read and distribute NMEA data in either real-time mode (for example, when streaming from a GPS device) or simulation mode (for example, when reading from a NMEA log file). In simulation mode, the source will emit updates according to the time stamp of each NMEA sentence to produce a &quot;replay&quot; of the recorded data.</p>
<a name="example-logfile-position-source-creating-a-custom-location-data-source"></a>
<h3>Example: Logfile Position Source - Creating a custom location data source</h3>
<p>Generally, the capabilities provided by the default position source as returned by QGeoPositionInfoSource::createSource(), along with the <a href="qnmeapositioninfosource.html">QNmeaPositionInfoSource</a> class, are sufficient for retrieving location data. However, in some cases developers may wish to write their own custom location data sources.</p>
<p>The <tt>LogFilePositionSource</tt> class in <tt>examples/logfilepositionsource</tt> shows how to subclass <a href="qgeopositioninfosource.html">QGeoPositionInfoSource</a> to create a custom location data source.</p>
<p>This example class reads location data from a text file, <i>log.txt</i>. The file specifies location data using a simple text format: it contains one location update per line, where each line contains a date/time, a latitude and a longitude, separated by spaces. The date/time is in ISO 8601 format and the latitude and longitude are in degrees decimal format. Here is an excerpt from <i>log.txt</i>:</p>
<pre>        2009-08-24T22:25:01 -27.576082 153.092415
        2009-08-24T22:25:02 -27.576223 153.092530
        2009-08-24T22:25:03 -27.576364 153.092648</pre>
<p>The class reads this data and distributes it via the <a href="qgeopositioninfosource.html#positionUpdated">positionUpdated()</a> signal.</p>
<p>Here is the definition of the <tt>LogFilePositionSource</tt> class:</p>
<pre>    class LogFilePositionSource : public QGeoPositionInfoSource
    {
        Q_OBJECT
    public:
        LogFilePositionSource(QObject *parent = 0);

        QGeoPositionInfo lastKnownPosition(bool fromSatellitePositioningMethodsOnly = false) const;

        PositioningMethods supportedPositioningMethods() const;
        int minimumUpdateInterval() const;

    public slots:
        virtual void startUpdates();
        virtual void stopUpdates();

        virtual void requestUpdate(int timeout = 5000);

    private slots:
        void readNextPosition();

    private:
        QFile *logFile;
        QTimer *timer;
        QGeoPositionInfo lastPosition;
    };</pre>
<p>The main methods overridden by the subclass are:</p>
<ul>
<li><a href="qgeopositioninfosource.html#startUpdates">startUpdates()</a>: called by client applications to start regular position updates</li>
<li><a href="qgeopositioninfosource.html#stopUpdates">stopUpdates()</a>: called by client applications to stop regular position updates</li>
<li><a href="qgeopositioninfosource.html#requestUpdate">requestUpdate()</a>: called by client applications to request a single update, with a specified timeout</li>
</ul>
<p>When a position update is available, the subclass emits the <a href="qgeopositioninfosource.html#positionUpdated">positionUpdated()</a> signal.</p>
<p>Here are the key methods in the class implementation:</p>
<pre>    LogFilePositionSource::LogFilePositionSource(QObject *parent)
        : QGeoPositionInfoSource(parent),
          logFile(new QFile(this)),
          timer(new QTimer(this))
    {
        connect(timer, SIGNAL(timeout()), this, SLOT(readNextPosition()));

        logFile-&gt;setFileName(QCoreApplication::applicationDirPath()
                + QDir::separator() + &quot;simplelog.txt&quot;);
        if (!logFile-&gt;open(QIODevice::ReadOnly))
            qWarning() &lt;&lt; &quot;Error: cannot open source file&quot; &lt;&lt; logFile-&gt;fileName();
    }

    void LogFilePositionSource::startUpdates()
    {
        int interval = updateInterval();
        if (interval &lt; minimumUpdateInterval())
            interval = minimumUpdateInterval();

        timer-&gt;start(interval);
    }

    void LogFilePositionSource::stopUpdates()
    {
        timer-&gt;stop();
    }

    void LogFilePositionSource::requestUpdate(int <span class="comment">/*timeout*/</span>)
    {
        <span class="comment">// For simplicity, ignore timeout - assume that if data is not available</span>
        <span class="comment">// now, no data will be added to the file later</span>
        if (logFile-&gt;canReadLine())
            readNextPosition();
        else
            emit requestTimeout();
    }

    void LogFilePositionSource::readNextPosition()
    {
        QByteArray line = logFile-&gt;readLine().trimmed();
        if (!line.isEmpty()) {
            QList&lt;QByteArray&gt; data = line.split(' ');
            double latitude;
            double longitude;
            bool hasLatitude = false;
            bool hasLongitude = false;
            QDateTime dateTime = QDateTime::fromString(QString(data.value(0)), Qt::ISODate);
            latitude = data.value(1).toDouble(&amp;hasLatitude);
            longitude = data.value(2).toDouble(&amp;hasLongitude);

            if (hasLatitude &amp;&amp; hasLongitude &amp;&amp; dateTime.isValid()) {
                QGeoCoordinate coordinate(latitude, longitude);
                QGeoPositionInfo info(coordinate, dateTime);
                if (info.isValid()) {
                    lastPosition = info;
                    emit positionUpdated(info);
                }
            }
        }
    }</pre>
<p>The example includes a <tt>ClientApplication</tt> class that requests updates from the <tt>LogFilePositionSource</tt> class. Run the exaple to see the data that is received by <tt>ClientApplication</tt>.</p>
<p>Before running the example, make sure you have done both <tt>make</tt> and <tt>make install</tt>.</p>
<p /><address><hr /><div align="center">
<table width="100%" cellspacing="0" border="0"><tr class="address">
<td align="left">Copyright &copy; 2009 Nokia Corporation and/or its subsidiary(-ies)</td>
<td align="right"><div align="right">Qt Location API</div></td>
</tr></table></div></address></body>
</html>
